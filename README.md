# Prérequis
- Installer WAMP sur la machine pour utiliser le projet en local avec PhpMyAdmin
- Créer un dossier observas sur la machine
- 
# Cloner le projet depuis GitLab
## En ligne de commande
- Aller dans le dossier observads et utiliser la commande suivante avec Git Bash: "git clone https://gitlab.com/erwan-scherrer-e4/observas.git"

# Configurer la base de données et la connexion
- Créer la base de données "2-ferre-tp" (base MySQL)
- Une fois la base créer utiliser le script "script-observas.sql" pour générer les tables et les données de test